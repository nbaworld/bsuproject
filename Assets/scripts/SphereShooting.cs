﻿using UnityEngine;
using System.Collections;

public class SphereShooting : MonoBehaviour
{
    public GameObject bullet;
    public AudioSource shot;
    public GameObject bulletSpawn;
    public float speed = 100f;

    void Start()
    {
        Cursor.visible = false;
    }

    void Update()
    {
       
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 worldPos = Camera.main.ScreenToWorldPoint(new Vector3((Screen.width + CrossHair.size) / 2, (Screen.height - CrossHair.size) / 2, Camera.main.nearClipPlane + 1.5f));
            Ray ray = new Ray(worldPos, Camera.main.transform.forward);
            //Debug.DrawRay(worldPos, Camera.main.transform.forward * 5, Color.green, 1);
            SpawnBullet();
            shot.Play();
            RaycastHit hit;
            /*if (Physics.Raycast(ray, out hit) && hit.transform.tag == "target")
            {
                DestroyObject(hit.transform.gameObject);
                
            }*/
        }
    }

    void SpawnBullet()
    {
        GameObject go = (GameObject)Instantiate(bullet, bulletSpawn.transform.position, bulletSpawn.transform.rotation);
        go.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * 3000);
        DestroyObject(go, 5);
    }
}
