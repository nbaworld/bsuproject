﻿using UnityEngine;
using System.Collections;

public class CrossHair : MonoBehaviour
{
    public Texture2D crosshair;
    public static float size = 25;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height / 2, size, size), crosshair);
    }
}
