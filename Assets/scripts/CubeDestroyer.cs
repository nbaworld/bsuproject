﻿using UnityEngine;
using System.Collections;

public class CubeDestroyer : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "bullet")
        {
            KillsCounter.killsCount++;
            DestroyObject(other.gameObject);
            DestroyObject(gameObject);
            
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
