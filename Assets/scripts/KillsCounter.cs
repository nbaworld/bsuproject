﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KillsCounter : MonoBehaviour {
    public static int killsCount = 0;
    Text txt;

	// Use this for initialization
	void Start () {
        txt = gameObject.GetComponent<Text>();
        txt.text = "Kills: 0";
    }
	
	// Update is called once per frame
	void Update () {
        txt.text = "Kills: " + killsCount.ToString();
    }
}
